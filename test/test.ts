import { expect, tap } from '@pushrocks/tapbundle';
import * as consenttuples from '../ts/index.js';

tap.test('first test', async () => {
  const transferableGoogleAnalyticsUniversalTuple =
    consenttuples.ConsentTuple.getGoogleAnalyticsUniversalTuple().getTransferableObject();
  console.log(transferableGoogleAnalyticsUniversalTuple);
});

tap.start();
